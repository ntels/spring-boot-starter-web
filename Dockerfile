FROM 112.160.104.138:38811/openjdk_curl:8-jdk-alpine

EXPOSE 8080

ADD target/starter-web-0.0.1.war starter-web.war

COPY entrypoint.sh /usr/local/bin/entrypoint.sh

RUN chmod +x /usr/local/bin/entrypoint.sh

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
