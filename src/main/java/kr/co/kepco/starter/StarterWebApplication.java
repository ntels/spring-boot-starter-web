package kr.co.kepco.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@SpringBootApplication
public class StarterWebApplication {

	@RequestMapping("/")
	public @ResponseBody String hello() {
		return "Hello World!";
	}
	
	public static void main(String[] args) {
		SpringApplication.run(StarterWebApplication.class, args);
	}

}
